create database Taller;
Use Taller;
-- PRIMERA TABLA
CREATE TABLE Clientes (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(50) NOT NULL,
  email VARCHAR(50) UNIQUE,
  fecha_nacimiento DATE,
  puntos_fidelidad INT DEFAULT 0
);

-- SEGUNDA TABLA 
CREATE TABLE Productos (
  id INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(50) NOT NULL,
  descripcion TEXT,
  precio DECIMAL(10, 2) NOT NULL,
  disponible BOOLEAN DEFAULT true
);
-- INSERTAR REGISTROS PRIMERA TABLA

INSERT INTO Clientes (nombre, email, fecha_nacimiento, puntos_fidelidad) VALUES
('Juan Pérez', 'juan@example.com', '1990-05-12', 20),
('María González', 'maria@example.com', '1985-10-23', 15),
('Pedro García', 'pedro@example.com', '1995-03-04', 10),
('Ana López', 'ana@example.com', '1992-08-17', 5),
('Carlos Ramírez', 'carlos@example.com', '1988-12-31', 0),
('Luisa Torres', 'luisa@example.com', '1998-06-08', 30),
('Sofía Rodríguez', 'sofia@example.com', '1983-02-14', 25),
('Fernando Martínez', 'fernando@example.com', '1979-11-27', 10),
('Lucía Sánchez', 'lucia@example.com', '1991-04-09', 20),
('Diego Jiménez', 'diego@example.com', '1994-09-01', 5);

-- SEGUNDA TABLA
INSERT INTO Productos (nombre, descripcion, precio, disponible) VALUES
('Camiseta blanca', 'Camiseta de algodón blanca para hombre', 19.99, true),
('Vestido negro', 'Vestido corto negro para mujer', 39.99, true),
('Zapatillas deportivas', 'Zapatillas para correr con suela de goma', 79.99, true),
('Pantalón vaquero', 'Pantalón vaquero de corte recto para hombre', 49.99, true),
('Bolso de mano', 'Bolso de mano de cuero para mujer', 89.99, false),
('Gorra de béisbol', 'Gorra de béisbol de algodón con visera', 9.99, true),
('Bufanda de lana', 'Bufanda de lana suave y cálida', 29.99, true),
('Pendientes de plata', 'Pendientes de plata con piedra de zafiro', 59.99, true),
('Reloj de pulsera', 'Reloj de pulsera con correa de cuero', 99.99, false),
('Sudadera con capucha', 'Sudadera con capucha para hombre', 39.99, true);

-- 5 QUERYS CLIENTES
SELECT * FROM Clientes ORDER BY nombre;
SELECT COUNT(*) AS total_clientes FROM Clientes;
SELECT * FROM Clientes WHERE puntos_fidelidad > 20;
UPDATE Clientes SET email = 'nuevo_correo@example.com' WHERE id = 3;
DELETE FROM Clientes WHERE id = 7;


-- 5 Querys PRODUCTOS
SELECT * FROM Productos WHERE disponible = true ORDER BY precio;
SELECT AVG(precio) AS precio_promedio FROM Productos;
SELECT * FROM Productos WHERE nombre LIKE '%vaquero%' OR descripcion LIKE '%vaquero%';
UPDATE Productos SET precio = 69.99 WHERE id = 4;
DELETE FROM Productos WHERE id = 8;
